package main

import (
	"fmt"
	"net/http"
	"ni_wi/api"
	"ni_wi/common"
	"ni_wi/frontend"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		common.NewSystemError("PORT variable must be set", nil)
	}
	// init db commection to mysql instance
	db := common.Init(os.Getenv("CLEARDB_DATABASE_URL"))

	defer db.Close()
	// set up api routes
	router := mux.NewRouter()

	// set up api routes
	api.APIRoutes(router)
	// set up frontend site routes
	frontend.SiteRoutes(router)
	// serve static files
	fs := http.FileServer(http.Dir("./frontend/assets/"))
	router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", fs))

	fmt.Printf("Servier served and listening on port %s", port)
	http.ListenAndServe(":"+port, router)
}
