package api

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"ni_wi/common"
	"ni_wi/model"
	"strings"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
)

var Store = sessions.NewCookieStore([]byte("shhhhhhwef!@#"))

func APIRoutes(router *mux.Router) {
	router.HandleFunc("/api/login", login).Methods("POST")
	router.HandleFunc("/api/signup", signup).Methods("POST")
	router.HandleFunc("/api/profile", updateUserProfile).Methods("POST")
	router.HandleFunc("/api/reset_password", resetPassword).Methods("POST")
	router.HandleFunc("/api/geolocation", getGeolocations).Methods("GET")
	// http.Handle("/api/profile", Middleware(router))
}

func login(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		common.NewSystemError("Error on user account request body read", err)
		return
	}

	var userRequest common.UserLoginRequest

	err = json.Unmarshal(body, &userRequest)
	if err != nil {
		common.NewSystemError("Error on user account request unmarshal", err)
		return
	}

	user, err := model.GetUserAccountByEmail(userRequest.Email)

	if user == nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "User account not found",
		}
		common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
		return
	}

	if err != nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: err.Error(),
		}
		common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
		return
	}

	// if account type is email compare the password
	if userRequest.Type == 1 {
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userRequest.Passowrd))

		if err != nil {
			loginResponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: "Incorrect password supplied",
			}
			common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
			return
		}
	}

	loginResponse := &common.UserRespone{
		Result:      "complete",
		SessionId:   user.ID.String(),
		ProfileEdit: true,
	}

	common.WriteJSONResponse(response, http.StatusOK, loginResponse)
	return
}

func signup(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		common.NewSystemError("Error on user account request body read", err)
		return
	}

	var signupRequest common.UserSignupRequest

	err = json.Unmarshal(body, &signupRequest)
	if err != nil {
		common.NewSystemError("Error on user account request unmarshal", err)
		return
	}

	err = validateSignupRequest(&signupRequest)

	if err != nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: err.Error(),
		}

		common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
		return
	}
	inUse := model.EmailInUse(signupRequest.Email)

	if inUse {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "Email address already in use",
		}

		common.WriteJSONResponse(response, http.StatusInternalServerError, loginResponse)
		return
	}

	err = model.CreateUserAccount(signupRequest.Username, signupRequest.Passowrd, signupRequest.Email, model.AccountType(signupRequest.Type), signupRequest.Sub)

	if err != nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "error on account create: " + err.Error(),
		}

		common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
		return
	}

	user, err := model.GetUserAccountByEmail(signupRequest.Email)

	if err != nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "error on account select: " + err.Error(),
		}

		common.WriteJSONResponse(response, http.StatusInternalServerError, loginResponse)
		return
	}

	err = user.InitUserProfile()

	if err != nil {
		loginResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "error on profile init: " + err.Error(),
		}

		common.WriteJSONResponse(response, http.StatusInternalServerError, loginResponse)
		return
	}

	loginResponse := &common.UserRespone{
		Result:      "complete",
		SessionId:   user.ID.String(),
		ProfileEdit: true,
	}

	common.WriteJSONResponse(response, http.StatusOK, loginResponse)
}

func resetPassword(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		common.NewSystemError("Error on user account request body read", err)
	}

	var passwordRequest common.ForgottenPasswordRequest

	err = json.Unmarshal(body, &passwordRequest)
	if err != nil {
		common.NewSystemError("Error on password request unmarshal", err)
	}

	// check if request is reqest password
	if passwordRequest.PasswordReset {
		requestToken, err := model.GetAccessToken(passwordRequest.Token)

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
		}

		// find the user account by via the request token struct
		user, err := model.GetUserAccountById(requestToken.UserID.String())

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
		}

		err = user.UpdateAccountPassword(passwordRequest.Password)

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
		}

	} else {
		user, err := model.GetUserAccountByEmail(passwordRequest.Email)

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
		}

		if user == nil {
			loginResponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: "Email account not found",
			}
			common.WriteJSONResponse(response, http.StatusBadRequest, loginResponse)
			return
		}

		// generate request token for user account
		requestToken, err := user.GenerateAccessToken()

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
			return
		}

		err = common.SendForgottenPasswordEmail(user.Email, requestToken)

		if err != nil {
			resetresponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, resetresponse)
			return
		}
	}

	profileResponse := &common.UserRespone{
		Result: "complete",
	}

	common.WriteJSONResponse(response, http.StatusOK, profileResponse)
}

func updateUserProfile(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		common.NewSystemError("Error on user account request body read", err)
	}

	var profileRequest common.UserProfileRequest

	err = json.Unmarshal(body, &profileRequest)
	if err != nil {
		common.NewSystemError("Error on user account request unmarshal", err)
	}

	user, err := model.GetUserAccountById(profileRequest.ID)

	if err != nil {
		profileResponse := &common.UserRespone{
			Result: "error on account select",
		}

		common.WriteJSONResponse(response, http.StatusInternalServerError, profileResponse)
	}

	err = user.Profile.UpdateUserProfile(profileRequest.Address, profileRequest.Telephone)

	if err != nil {
		profileResponse := &common.UserRespone{
			Result:       "error",
			ErrorMessage: "error on adding user profile: " + err.Error(),
		}

		common.WriteJSONResponse(response, http.StatusInternalServerError, profileResponse)
		return
	}

	if profileRequest.Email != user.Email {
		err := user.UpdateAccountEmail(profileRequest.Email)

		if err != nil {
			profileResponse := &common.UserRespone{
				Result:       "error",
				ErrorMessage: "error on change on account email: " + err.Error(),
			}

			common.WriteJSONResponse(response, http.StatusInternalServerError, profileResponse)
			return
		}
	}

	profileResponse := &common.UserRespone{
		Result: "complete",
	}

	common.WriteJSONResponse(response, http.StatusOK, profileResponse)
}

func getGeolocations(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	query := request.URL.Query().Get("query")

	if strings.Contains(query, " ") {
		query = strings.ReplaceAll(query, " ", "+")
	}

	// retrieve geolocation locations from google automcomplete api
	resp, err := http.Get("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&key=AIzaSyCnFq-Pj5DoulYXcxWp1IVMqpfZBjxJ6Ic")
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	var geolocationResponse common.GeolocationResponse

	err = json.Unmarshal(body, &geolocationResponse)

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	common.WriteJSONResponse(response, http.StatusOK, geolocationResponse)
}

func validateLoginRequest(request *common.UserLoginRequest) error {
	if request == nil {
		return common.NewSystemError("Mising request payload", nil)
	}

	return nil
}

func validateSignupRequest(request *common.UserSignupRequest) error {
	if request == nil {
		return common.NewSystemError("Mising request payload", nil)
	}
	if len(request.Username) == 0 && request.Type == 1 {
		return common.NewSystemError("Username must not be blank", nil)
	}
	if len(request.Email) == 0 {
		return common.NewSystemError("Email must not be blank", nil)
	}
	if len(request.Passowrd) < 8 && request.Type == 1 {
		return common.NewSystemError("Password must be more than 8 characters long", nil)
	}

	return nil
}
