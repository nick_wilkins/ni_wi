package common

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var conf *oauth2.Config
var state string

func InitGoogleAPI() {
	clientId := os.Getenv("g_api_client_id")
	clientSecret := os.Getenv("g_api_client_secret")
	apiURL := os.Getenv("API_URL")

	conf = &oauth2.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		RedirectURL:  fmt.Sprintf("%s/auth", apiURL),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

}

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func GetLoginURL() string {
	return conf.AuthCodeURL(randToken())
}

func AuthHandler(code string) ([]byte, error) {
	var data []byte

	tok, err := conf.Exchange(oauth2.NoContext, code)
	if err != nil {
		return data, NewSystemError("error parsing provided code", err)
	}
	// Construct the client.
	client := conf.Client(oauth2.NoContext, tok)

	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return data, NewSystemError("error on google api get", err)
	}

	defer resp.Body.Close()
	data, _ = ioutil.ReadAll(resp.Body)

	log.Println("Email body: ", string(data))

	return data, nil
}
