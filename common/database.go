package common

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Database struct {
	*sql.DB
}

var DB *sql.DB

// Opening a database and save the reference to `Database` struct.
func Init(url string) *sql.DB {
	db, err := sql.Open("mysql", "b5e3677374e406:d5a3f5b6@tcp(us-cdbr-east-02.cleardb.com)/heroku_2763138f6ecdde2")

	if err != nil {
		// if error on connect throw error and return
		NewSystemError("Error on db connect", err)
		return nil
	}

	DB = db
	return DB
}

// Using this function to get a connection, you can create your connection pool here.
func GetDB() *sql.DB {
	return DB
}
