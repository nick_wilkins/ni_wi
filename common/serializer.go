package common

import (
	"encoding/json"
	"net/http"
	"strconv"
)

type UserSignupRequest struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Passowrd string `json:"password"`
	Sub      string `json:"sub"`
	Type     int    `json:"type"`
}

type UserAccountRequest struct {
	Username string `json:"user_name"`
	Email    string `json:"email"`
	Passowrd string `json:"password"`
}

type UserLoginRequest struct {
	Email    string `json:"email"`
	Passowrd string `json:"password"`
	Type     int    `json:"type"`
}

type ForgottenPasswordRequest struct {
	Email         string `json:"email"`
	PasswordReset bool   `json:"reset_password"`
	Password      string `json:"password"`
	Token         string `json:"token"`
}

type UserProfileRequest struct {
	ID        string `json:"id"`
	Email     string `json:"email"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	Editable  bool   `json:"is_editable"`
}

type UserRespone struct {
	Result       string `json:"result"`
	ErrorMessage string `json:"error_msg"`
	SessionId    string `json:"session_id"`
	ProfileEdit  bool   `json:"profile_edit"`
}

type GeolocationResponse struct {
	Predictions []GeolocationPredictions `json:"predictions"`
}

type GeolocationPredictions struct {
	Description string `json:"description"`
	ID          string `json:"id"`
}

func WriteJSONResponse(w http.ResponseWriter, status int, object interface{}) {
	data, _ := json.Marshal(&object)
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	w.Header().Set("Connection", "close")
	w.WriteHeader(status)
	w.Write(data)
}

// func SetErrorResponse(res http.ResponseWriter, error error) {
// 	customError := common.ToError(error)
// 	res.WriteHeader(400)
// 	res.Write(buildErrorPayload(customError.Mesg))
// }

// func BadRequest(res http.ResponseWriter, errorString string) {
// 	res.WriteHeader(http.StatusBadRequest)
// 	res.Write(buildErrorPayload(errorString))
// }
