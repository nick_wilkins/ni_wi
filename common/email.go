package common

import (
	"net/smtp"
	"os"
)

func SendForgottenPasswordEmail(emailAddress string, code string) error {
	from := "nickwilkins92@gmail.com"
	pass := os.Getenv("EMAIL_PASS")
	to := "nickawilkins@hotmail.com"

	msg := []byte("Content-Type: text/html;'\r\n" +
		"From: 'Password Reset' <" + from + "> \n" +
		"To: " + to + "\n" +
		"Subject: Password reset \r\n" +
		"\r\n Please follow the link to reset your account password \r\n" +
		"\r\n <a href='" + os.Getenv("API_URL") + "/reset-password?token=" + code + "'>Reset Password</a>")

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, msg)

	if err != nil {
		return NewSystemError("smtp error", err)
	}

	return nil

}
