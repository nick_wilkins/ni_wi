package common

import (
	"fmt"
)

type ErrorType int

const (
	InvalidParameterValue ErrorType = 0
	MissingParameterValue ErrorType = 1
	Authentication        ErrorType = 2
	Authorization         ErrorType = 3
	EntityAlreadyExists   ErrorType = 4
	NoRecordsFound        ErrorType = 5
	System                ErrorType = 6
)

type SystemError struct {
	Type    ErrorType
	Message string
	Code    int
}

func (e *SystemError) Error() string {
	if e == nil {
		return "Error is not set"
	}
	return fmt.Sprintf("Error code %d , message: %s", e.Type, e.Message)
}

func NewSystemError(errorMessage string, err error) *SystemError {
	return newError(System, errorMessage, err, 500)
}

func newError(errorType ErrorType, errorMessage string, err error, errorCode int) *SystemError {
	var mesg string
	if err != nil {
		mesg = fmt.Sprintf("%s: %v", errorMessage, err)
	} else {
		mesg = errorMessage
	}

	fmt.Println(mesg)

	return &SystemError{
		Type:    errorType,
		Message: mesg,
		Code:    errorCode,
	}
}
