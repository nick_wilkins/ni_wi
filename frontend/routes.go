package frontend

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"ni_wi/api"
	"ni_wi/common"
	"ni_wi/model"
	"os"

	"github.com/gorilla/mux"
)

type Profile struct {
	IsAuthenticated bool
	EditMode        bool
	Username        string
	Email           string
	Address         string
	Telephone       string
}

type Login struct {
	AuthURL      string
	HasErrored   bool
	ErrorMessage string
}

type PasswordReset struct {
	HasErrored    bool
	ErrorMessage  string
	ResetPassword bool
}

func SiteRoutes(router *mux.Router) {
	router.HandleFunc("/", mainHanlder).Methods("GET")
	router.HandleFunc("/signup", signupHanlder).Methods("GET")
	router.HandleFunc("/profile", profileHandler).Methods("GET")
	router.HandleFunc("/reset-password", forgotPassHanlder).Methods("GET")
	router.HandleFunc("/auth", auth).Methods("GET")
	router.HandleFunc("/logout", logoutHandler).Methods("GET")

	// configure form POST routes for site
	router.HandleFunc("/", loginPostHandler).Methods("POST")
	router.HandleFunc("/signup", signupPostHandler).Methods("POST")
	router.HandleFunc("/profile", profilePostHandler).Methods("POST")
	router.HandleFunc("/reset-password", forgotPassPostHanlder).Methods("POST")
}

func auth(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	query := request.URL.Query()
	code := query.Get("code")

	common.InitGoogleAPI()
	data, err := common.AuthHandler(code)

	if err != nil {
		http.Error(response, "issue with user auth token", http.StatusBadRequest)
		return
	}

	var userDetails model.GoogleUserDetails

	err = json.Unmarshal(data, &userDetails)

	if err != nil {
		http.Error(response, "error un-marshaling data into google details", http.StatusBadRequest)
		return
	}

	signupRequest, err := json.Marshal(&common.UserSignupRequest{
		Username: userDetails.Name,
		Email:    userDetails.Email,
		Sub:      userDetails.Sub,
		Type:     2,
	})

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	var loginResponse common.UserRespone

	if model.ValidateGoogleAuth(userDetails.Sub, userDetails.Email) {

		loginRequest, err := json.Marshal(&common.UserLoginRequest{
			Email: userDetails.Email,
			Type:  2,
		})

		resp, err := http.Post(fmt.Sprintf("%s/api/login", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(loginRequest))
		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		json.Unmarshal(body, &loginResponse)
	} else {
		resp, err := http.Post(fmt.Sprintf("%s/api/signup", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(signupRequest))
		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		json.Unmarshal(body, &loginResponse)
	}

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	if loginResponse.SessionId != "" && loginResponse.Result == "complete" {
		session.Values["id"] = loginResponse.SessionId
		session.Values["edit_profile"] = true

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/profile", http.StatusSeeOther)
	} else {
		session.Values["has_error"] = true
		session.Values["error_msg"] = loginResponse.ErrorMessage

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/signup", http.StatusSeeOther)
	}
	return
}

func mainHanlder(response http.ResponseWriter, request *http.Request) {
	loginTmpl := template.Must(template.New("login").Funcs(template.FuncMap{}).ParseFiles("frontend/login.html"))

	common.InitGoogleAPI()
	url := common.GetLoginURL()

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	var hasError bool = false
	var errorMessage string = ""

	sessionHasError := session.Values["has_error"]
	sessionErrorMessage := session.Values["error_msg"]

	if sessionHasError != nil {
		hasError = sessionHasError.(bool)
	}

	if sessionErrorMessage != nil {
		errorMessage = sessionErrorMessage.(string)
	}

	login := &Login{
		AuthURL:      url,
		HasErrored:   hasError,
		ErrorMessage: errorMessage,
	}

	loginTmpl.ExecuteTemplate(response, "login", login)

	http.Redirect(response, request, "/profile", http.StatusSeeOther)
	return
}

func loginPostHandler(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	email := request.FormValue("email")
	password := request.FormValue("password")

	loginRequest, err := json.Marshal(&common.UserLoginRequest{
		Email:    email,
		Passowrd: password,
		Type:     1,
	})

	if err != nil {
		print(err)
	}

	resp, err := http.Post(fmt.Sprintf("%s/api/login", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(loginRequest))
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	var loginResponse common.UserRespone

	json.Unmarshal(body, &loginResponse)

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	if loginResponse.SessionId != "" && loginResponse.Result == "complete" {
		session.Values["id"] = loginResponse.SessionId
		session.Values["edit_profile"] = false

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/profile", http.StatusSeeOther)
	} else {
		session.Values["has_error"] = true
		session.Values["error_msg"] = loginResponse.ErrorMessage

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/", http.StatusSeeOther)
	}
	return
}

func signupHanlder(response http.ResponseWriter, request *http.Request) {
	signupTmpl := template.Must(template.New("signup").ParseFiles("frontend/signup.html"))

	common.InitGoogleAPI()
	url := common.GetLoginURL()

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	var hasError bool = false
	var errorMessage string = ""

	sessionHasError := session.Values["has_error"]
	sessionErrorMessage := session.Values["error_msg"]

	if sessionHasError != nil {
		hasError = sessionHasError.(bool)
	}

	if sessionErrorMessage != nil {
		errorMessage = sessionErrorMessage.(string)
	}

	signup := &Login{
		AuthURL:      url,
		HasErrored:   hasError,
		ErrorMessage: errorMessage,
	}

	signupTmpl.ExecuteTemplate(response, "signup", signup)
}

func signupPostHandler(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	username := request.FormValue("username")
	email := request.FormValue("email")
	password := request.FormValue("password")

	signupRequest, err := json.Marshal(&common.UserSignupRequest{
		Username: username,
		Email:    email,
		Passowrd: password,
		Type:     1,
	})

	if err != nil {
		print(err)
	}

	resp, err := http.Post(fmt.Sprintf("%s/api/signup", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(signupRequest))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	var loginResponse common.UserRespone

	json.Unmarshal(body, &loginResponse)

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	if loginResponse.SessionId != "" && loginResponse.Result == "complete" {
		session.Values["id"] = loginResponse.SessionId
		session.Values["edit_profile"] = true

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/profile", http.StatusSeeOther)
	} else {
		session.Values["has_error"] = true
		session.Values["error_msg"] = loginResponse.ErrorMessage

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/signup", http.StatusSeeOther)
	}
	return
}

func forgotPassHanlder(response http.ResponseWriter, request *http.Request) {
	forgotPassTmpl := template.Must(template.New("signup").ParseFiles("frontend/forgotpassword.html"))

	query := request.URL.Query()
	token := query.Get("token")

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	var hasError bool = false
	var errorMessage string = ""
	var passwordReset bool = false

	sessionHasError := session.Values["has_error"]
	sessionErrorMessage := session.Values["error_msg"]

	if token != "" {
		passwordReset = true

		session.Values["reset_token"] = token
		session.Save(request, response)
	}

	if sessionHasError != nil {
		hasError = sessionHasError.(bool)
	}

	if sessionErrorMessage != nil {
		errorMessage = sessionErrorMessage.(string)
	}

	resetPassword := &PasswordReset{
		HasErrored:    hasError,
		ErrorMessage:  errorMessage,
		ResetPassword: passwordReset,
	}

	forgotPassTmpl.ExecuteTemplate(response, "forgotpassword", resetPassword)
}

func forgotPassPostHanlder(response http.ResponseWriter, request *http.Request) {
	email := request.FormValue("email")
	password := request.FormValue("confirmPassword")

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	var isReset bool = false
	var resquestToken string

	token := session.Values["reset_token"]

	if token != nil && token != "" {
		resquestToken = token.(string)
		isReset = true
	}

	passwordReset, err := json.Marshal(&common.ForgottenPasswordRequest{
		Email:         email,
		PasswordReset: isReset,
		Password:      password,
		Token:         resquestToken,
	})

	resp, err := http.Post(fmt.Sprintf("%s/api/reset_password", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(passwordReset))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	var apiResponse common.UserRespone

	json.Unmarshal(body, &apiResponse)

	if apiResponse.Result == "complete" {
		http.Redirect(response, request, "/", http.StatusSeeOther)
	} else {
		session.Values["has_error"] = true
		session.Values["error_msg"] = apiResponse.ErrorMessage

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/reset-password", http.StatusSeeOther)
	}
	return
}

func profileHandler(w http.ResponseWriter, r *http.Request) {
	var profile *Profile
	profileTmpl := template.Must(template.New("profile").ParseFiles("frontend/profile.html"))

	session, err := api.Store.Get(r, "session")

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var is_editable bool = false

	id := session.Values["id"]
	edit_profile := session.Values["edit_profile"]

	if edit_profile != nil {
		is_editable = edit_profile.(bool)
	}

	if id != nil {
		user, _ := model.GetUserAccountById(id.(string))

		profile = &Profile{
			IsAuthenticated: true,
			EditMode:        is_editable,
			Username:        user.Username,
			Email:           user.Email,
			Address:         user.Profile.Address,
			Telephone:       user.Profile.Telephone,
		}
	} else {

		profile = &Profile{
			IsAuthenticated: false,
			EditMode:        is_editable,
			Username:        "",
			Email:           "",
			Address:         "",
			Telephone:       "",
		}
	}

	profileTmpl.ExecuteTemplate(w, "profile", profile)
}

func profilePostHandler(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")

	email := request.FormValue("email")
	address := request.FormValue("address")
	telephone := request.FormValue("telephone")

	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	edit_profile := session.Values["edit_profile"]

	if edit_profile != nil && !edit_profile.(bool) {
		session.Values["edit_profile"] = true

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/profile", http.StatusSeeOther)
		return
	}

	id := session.Values["id"]

	profileRequest, err := json.Marshal(&common.UserProfileRequest{
		ID:        id.(string),
		Email:     email,
		Address:   address,
		Telephone: telephone,
	})

	resp, err := http.Post(fmt.Sprintf("%s/api/profile", os.Getenv("API_URL")), "application/json", bytes.NewBuffer(profileRequest))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}

	var profileResponse common.UserRespone

	json.Unmarshal(body, &profileResponse)

	if profileResponse.Result == "complete" {
		session.Values["edit_profile"] = false

		err = session.Save(request, response)

		if err != nil {
			http.Error(response, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(response, request, "/profile", http.StatusSeeOther)
	}
}

// clear down stored session on logout
func logoutHandler(response http.ResponseWriter, request *http.Request) {
	session, err := api.Store.Get(request, "session")

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["edit_profile"] = nil
	session.Values["id"] = nil
	session.Values["has_error"] = nil
	session.Values["error_msg"] = nil

	err = session.Save(request, response)

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(response, request, "/", http.StatusSeeOther)
}
