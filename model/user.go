package model

import (
	"ni_wi/common"

	"github.com/gofrs/uuid"
	"golang.org/x/crypto/bcrypt"
)

type AccountStatus int

const (
	AccountStatus_NOT_SET  AccountStatus = 0
	AccountStatus_ACTIVE   AccountStatus = 1
	AccountStatus_INACTIVE AccountStatus = 2
)

type AccountType int

const (
	AccountType_NOT_SET AccountType = 0
	AccountType_EMAIL   AccountType = 1
	AccountType_GOOGLE  AccountType = 2
)

type UserAccount struct {
	ID       uuid.UUID
	Username string
	Password string
	Email    string
	Status   AccountStatus
	Type     AccountType
	Sub      string
	Profile  *UserProfile
}

type UserProfile struct {
	ID        uuid.UUID
	UserID    uuid.UUID
	FullName  string
	Address   string
	Telephone string
}

type RequestTokens struct {
	ID     uuid.UUID
	UserID uuid.UUID
	Token  uuid.UUID
	Valid  int
}

type GoogleUserDetails struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
}

func CreateUserAccount(username string, password string, email string, accountType AccountType, sub string) error {
	db := common.GetDB()
	// var userAccount UserAccount
	var hashedPassword []byte

	query, err := db.Prepare("insert into user_account(id, username, password, email, status, type, sub) values (?, ?, ?, ?, ?, ?, ?)")

	if err != nil {
		return common.NewSystemError("Error on user insert", err)
	}

	if accountType == AccountType_GOOGLE {
		password = common.RandString(50)
	}

	hashedPassword, err = bcrypt.GenerateFromPassword([]byte(password), 8)
	if err != nil {
		return common.NewSystemError("Failed to create hashpassword", err)
	}

	uuid, _ := uuid.NewV4()

	_, err = query.Exec(uuid, username, hashedPassword, email, 1, accountType, sub)

	if err != nil {
		return common.NewSystemError("Failed to insert user account", err)
	}

	return nil
}

func GetUserAccountById(id string) (UserAccount, error) {
	db := common.GetDB()
	var user UserAccount

	userId, err := uuid.FromString(id)

	if err != nil {
		return user, common.NewSystemError("Error converting string to uuid", err)
	}

	result, err := db.Query("select id, username, password, email, status from user_account where id = ?", userId)

	if err != nil {
		return user, common.NewSystemError("Error on user select", err)
	}

	defer result.Close()

	for result.Next() {
		err := result.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Status)
		if err != nil {
			return user, common.NewSystemError("Error populating user struct", err)
		}
	}

	user.GetUserProfile()

	return user, nil
}

func GetUserAccountByUsername(username string) (UserAccount, error) {
	db := common.GetDB()
	var user UserAccount

	result, err := db.Query("select id, username, password, email, status from user_account where username = ?", username)

	if err != nil {
		return user, common.NewSystemError("Error on user select", err)
	}

	defer result.Close()

	for result.Next() {
		err := result.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Status)
		if err != nil {
			return user, common.NewSystemError("Error populating user struct", err)
		}
	}

	user.GetUserProfile()

	return user, nil
}

func GetUserAccountByEmail(email string) (*UserAccount, error) {
	db := common.GetDB()
	var user UserAccount

	result, err := db.Query("select id, username, password, email, status from user_account where email = ?", email)

	if err != nil {
		return &user, common.NewSystemError("Error on user select", err)
	}

	defer result.Close()

	rowCount := 0

	for result.Next() {
		rowCount = rowCount + 1
		err := result.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Status)
		if err != nil {
			return &user, common.NewSystemError("Error populating user struct", err)
		}
	}

	if rowCount == 0 {
		return nil, nil
	}

	user.GetUserProfile()

	return &user, nil
}

func GetUserAccountBySub(sub string) (*UserAccount, error) {
	db := common.GetDB()
	var user UserAccount

	result, err := db.Query("select * from user_account where sub = ?", sub)

	if err != nil {
		return &user, common.NewSystemError("Error on user select", err)
	}

	defer result.Close()

	for result.Next() {
		err := result.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.Status, &user.Type, &user.Sub)
		if err != nil {
			return &user, common.NewSystemError("Error populating user struct", err)
		}
	}

	return &user, nil
}

func EmailInUse(email string) bool {
	db := common.GetDB()
	var count int
	row := db.QueryRow("select count(id) from user_account where email = ?", email)

	if err := row.Scan(&count); err != nil || count == 0 {
		return false
	}

	return true
}

func ValidateGoogleAuth(sub string, email string) bool {
	db := common.GetDB()
	var count int
	row := db.QueryRow("select count(id) from user_account where email = ? and sub = ? and type = 2", email, sub)

	if err := row.Scan(&count); err != nil || count == 0 {
		return false
	}

	return true
}

// Get user profile information
func (user *UserAccount) UpdateAccountEmail(email string) error {
	db := common.GetDB()
	// update user profile address and telephone
	query, err := db.Prepare("update user_account set email = ? where id = ?")

	if err != nil {
		return common.NewSystemError("Error on user profile insert prepare", err)
	}

	_, err = query.Exec(email, user.ID)

	if err != nil {
		return common.NewSystemError("Error on user profile update", err)
	}

	return nil
}

// Update user account password
func (user *UserAccount) UpdateAccountPassword(password string) error {
	db := common.GetDB()
	// update user profile address and telephone
	query, err := db.Prepare("update user_account set password = ? where id = ?")

	if err != nil {
		return common.NewSystemError("Error on user profile insert prepare", err)
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 8)
	if err != nil {
		return common.NewSystemError("Failed to create hashpassword", err)
	}

	_, err = query.Exec(hashedPassword, user.ID)

	if err != nil {
		return common.NewSystemError("Error on user profile update", err)
	}

	return nil
}

// Get user profile information
func (user *UserAccount) GetUserProfile() error {
	db := common.GetDB()
	var profile UserProfile

	result, err := db.Query("select * from user_profile where user_id = ?", user.ID)

	if err != nil {
		return common.NewSystemError("Error on user profile select", err)
	}

	defer result.Close()

	for result.Next() {
		err := result.Scan(&profile.ID, &profile.UserID, &profile.FullName, &profile.Address, &profile.Telephone)
		if err != nil {
			return common.NewSystemError("Error populating user struct", err)
		}
	}

	// apply profile to user struct
	user.Profile = &profile

	return nil
}

// set up the user profile for the user account
func (user *UserAccount) InitUserProfile() error {
	db := common.GetDB()

	query, err := db.Prepare("insert into user_profile(id, user_id) values (?, ?)")

	if err != nil {
		return common.NewSystemError("Error on user profile insert prepare", err)
	}

	uuid, _ := uuid.NewV4()

	_, err = query.Exec(uuid, user.ID)

	if err != nil {
		return common.NewSystemError("Error on user profile insert exec", err)
	}

	return nil
}

func (profile *UserProfile) UpdateUserProfile(address string, telephone string) error {
	db := common.GetDB()

	// update user profile address and telephone
	query, err := db.Prepare("update user_profile set address = ?, telephone = ? where id = ?")

	if err != nil {
		return common.NewSystemError("Error on user profile insert prepare", err)
	}

	_, err = query.Exec(address, telephone, profile.ID)

	if err != nil {
		return common.NewSystemError("Error on user profile update", err)
	}

	return nil
}

// create access token for password reset
func (user *UserAccount) GenerateAccessToken() (string, error) {
	db := common.GetDB()
	var token string

	query, err := db.Prepare("insert into request_token(id, user_id, token, valid) values (?, ?, ?, ?)")

	if err != nil {
		return token, common.NewSystemError("Error on user profile insert prepare", err)
	}

	uuidToken, _ := uuid.NewV4()
	uuid, _ := uuid.NewV4()

	_, err = query.Exec(uuid, user.ID, uuidToken, 1)

	if err != nil {
		return token, common.NewSystemError("Error on user profile insert exec", err)
	}

	token = uuidToken.String()

	return token, nil
}

// Get password reset token
func GetAccessToken(token string) (RequestTokens, error) {
	db := common.GetDB()
	var reqToken RequestTokens

	result, err := db.Query("select user_id, valid from request_token where token = ?", token)

	if err != nil {
		return reqToken, common.NewSystemError("Error on request token select", err)
	}

	defer result.Close()

	for result.Next() {
		err := result.Scan(&reqToken.UserID, &reqToken.Valid)
		if err != nil {
			return reqToken, common.NewSystemError("Error populating token struct", err)
		}
	}

	return reqToken, nil
}

// Get password reset token
func (token *RequestTokens) SetTokenToInvalid() error {
	db := common.GetDB()

	result, err := db.Query("update request_token set valid = 0 where id = ?", token.ID)

	if err != nil {
		return common.NewSystemError("Error on request token select", err)
	}

	defer result.Close()

	return nil
}
