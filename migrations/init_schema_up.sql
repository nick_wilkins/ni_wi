CREATE TABLE IF NOT EXISTS user_account
(
    id    VARCHAR(50)  PRIMARY KEY,
    username VARCHAR(50)  NOT NULL,
    password VARCHAR(500) NOT NULL,
    email    VARCHAR(50)  NOT NULL,
    status int NOT NULL,
    type int NOT NULL,
    sub archar(100) NOT NULL
);


CREATE TABLE IF NOT EXISTS user_profile
(
    id   VARCHAR(50) PRIMARY KEY,
    user_id VARCHAR(50) not null,
    full_name varchar(50) not null,
    address varchar(200) not null,
    telephone varchar(20) not null
);

CREATE TABLE IF NOT EXISTS request_token
(
    id   VARCHAR(50) PRIMARY KEY,
    user_id VARCHAR(50) not null,
    token varchar(50) not null,
    valid TINYINT(1)  not null,
);